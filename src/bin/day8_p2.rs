// Day 8: Treetop Tree House (part 2)
//
// Find the tree that can see (is surrounded by trees of < height) the most number of other trees
// in the grid
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day8/input"
    } else {
        "assets/day8/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut grid = Vec::<Vec<u8>>::new();

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        grid.push(
            line.chars().map(|c| c.to_digit(10).unwrap() as u8)
            .collect::<Vec<u8>>());
    }

    let mut val_max = 0;


    for y in 1..(grid.len()-1) {
        for x in 1..(grid[0].len()-1) {
            let ref_tree_h = grid[y][x];

            let mut val = 1;
            let mut val_d = 0;
            for y_p in (0..y).rev() {
                val_d += 1;
                if grid[y_p][x] >= ref_tree_h {
                    break;
                }
            }

            val *= val_d;
            val_d = 0;

            for y_p in (y+1)..grid.len() {
                val_d += 1;
                if grid[y_p][x] >= ref_tree_h {
                    break;
                }
            }
            val *= val_d;
            val_d = 0;

            for x_p in (0..x).rev() {
                val_d += 1;
                if grid[y][x_p] >= ref_tree_h {
                    break;
                }
            }

            val *= val_d;
            val_d = 0;

            for x_p in (x+1)..grid[0].len() {
                val_d += 1;
                if grid[y][x_p] >= ref_tree_h {
                    break;
                }
            }

            val *= val_d;

            if val > val_max {
                val_max = val;
            }
        }
    }

    println!("highest scenic score: {}", val_max);
}
