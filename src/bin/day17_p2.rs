// Day 17: Pyroclastic Flow (part 2)
//
// Find height of list of Tetris shaped rocks after 1000000000000 rocks have stopped falling
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
enum Jet {
    Left,
    Right,
}

impl Jet {
    fn from_char(c: char) -> Result<Self, ()> {
        match c {
            '<' => Ok(Jet::Left),
            '>' => Ok(Jet::Right),
            _ => Err(()),
        }
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day17/input"
    } else {
        "assets/day17/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut jets = Vec::<Jet>::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        for c in line.chars() {
            jets.push(Jet::from_char(c).unwrap());
        }
    }

    let mut chamber = Vec::<u8>::new();
    chamber.push(0b11111111); // adding the floor

    let jet_periodicity = jets.len() as u64;
    let mut jets = jets.iter().cycle();

    let s1 = vec![0b00111100u8];
    let s2 = vec![0b00010000u8, 0b00111000u8, 0b00010000u8];
    let s3 = vec![0b00111000u8, 0b00001000u8, 0b00001000u8];
    let s4 = vec![0b00100000u8, 0b00100000u8, 0b00100000u8, 0b00100000u8];
    let s5 = vec![0b00110000u8, 0b00110000u8];

    let shape_sequence = vec![s1, s2, s3, s4, s5];
    let mut shape_iterator = shape_sequence.iter().cycle();

    let mut num_shapes_fallen = 0u64;

    let mut jet_num = 0u64;

    let mut cumulative_mask = 0u8;

    let mut repeat_markers = Vec::<(Vec<u8>, u64, u64, u64, u64)>::new();
    let mut repeat_start = 0;
    let mut repeat_length = 0;
    let mut repeat_delta_h = 0;
    let mut cached_heights = Vec::<u64>::new();

    'outerouter: while num_shapes_fallen < 100000000 {
        let mut next_shape = shape_iterator.next().unwrap().clone();

        'outer: for _ in 0..3 {
            jet_num += 1;
            match jets.next().unwrap() {
                Jet::Left => {
                    for r in next_shape.iter() {
                        if r & 0b10000000u8 != 0 {
                            continue 'outer;
                        }
                    }

                    for r in next_shape.iter_mut() {
                        *r = *r << 1;
                    }
                }
                Jet::Right => {
                    for r in next_shape.iter() {
                        if r & 0b00000010u8 != 0 {
                            continue 'outer;
                        }
                    }

                    for r in next_shape.iter_mut() {
                        *r = *r >> 1;
                    }
                }
            }
        }

        let mut shape_height = chamber.len();

        'outer: loop {
            let mut fall_complete = false;

            jet_num += 1;
            match jets.next().unwrap() {
                Jet::Left => {
                    let mut can_move = true;
                    for (i, r) in next_shape.iter().enumerate() {
                        if r & 0b10000000u8 != 0 {
                            can_move = false;
                        }

                        if shape_height + i < chamber.len()
                            && (*r << 1) & chamber[shape_height + i] != 0
                        {
                            can_move = false;
                        }
                    }

                    if can_move {
                        for r in next_shape.iter_mut() {
                            *r = *r << 1;
                        }
                    }
                }
                Jet::Right => {
                    let mut can_move = true;
                    for (i, r) in next_shape.iter().enumerate() {
                        if r & 0b00000010u8 != 0 {
                            can_move = false;
                        }

                        if shape_height + i < chamber.len()
                            && (*r >> 1) & chamber[shape_height + i] != 0
                        {
                            can_move = false;
                        }
                    }

                    if can_move {
                        for r in next_shape.iter_mut() {
                            *r = *r >> 1;
                        }
                    }
                }
            }

            shape_height -= 1;

            'inner: for (i, r) in next_shape.iter().enumerate() {
                if shape_height + i >= chamber.len() {
                    break 'inner;
                }
                if *r & chamber[shape_height + i] != 0 {
                    fall_complete = true;
                    break 'inner;
                }
            }

            if fall_complete {
                for (i, r) in next_shape.iter().enumerate() {
                    if shape_height + i + 1 < chamber.len() {
                        chamber[shape_height + i + 1] |= r;
                        cumulative_mask |= r;
                    } else {
                        chamber.push(*r);
                        cumulative_mask |= r;
                    }
                }

                // check the top of our current rock pile against previously found
                // 'repeat_markers'. If we find a matching repeat_marker and have the
                // same next shape and are at the same point in the cycle of jets then
                // we know that the simulation will just repeat from here on in
                for repeat_marker in repeat_markers.iter() {
                    if num_shapes_fallen % 5 == repeat_marker.2 % 5
                        && jet_num % jet_periodicity == repeat_marker.1
                    {
                        if chamber[chamber.len() - repeat_marker.0.len()..] == repeat_marker.0[..] {
                            repeat_start = repeat_marker.2;
                            repeat_length = num_shapes_fallen - repeat_start;
                            let repeat_start_h = repeat_marker.3;
                            repeat_delta_h = chamber.len() as u64 - 1 - repeat_start_h;

                            cached_heights.push(chamber.len() as u64 - 1);

                            break 'outerouter;
                        }
                    }
                }

                // pieces have fallen such that they provide a complete coverage of the chamber.
                // Find the shortest sequence from the current top of the rock pile that provides
                // this complete coverage and save details as a 'repeat_marker'. Will check in
                // subsequent runs for these repeat_markers to see if we have reached a point in
                // the simulation where it repeats itself
                if cumulative_mask == 0b11111110 {
                    cumulative_mask = 0;
                    let mut repeat_marker_len = 0;
                    for l in chamber.iter().rev() {
                        repeat_marker_len += 1;
                        cumulative_mask |= l;
                        if cumulative_mask == 0b11111110 {
                            break;
                        }
                    }

                    repeat_markers.push((
                        chamber[chamber.len() - repeat_marker_len..]
                            .iter()
                            .map(|x| *x)
                            .collect::<Vec<_>>(),
                        jet_num % jet_periodicity,
                        num_shapes_fallen,
                        chamber.len() as u64 - 1,
                        repeat_marker_len as u64,
                    ));
                    cumulative_mask = 0;
                }

                num_shapes_fallen += 1;
                cached_heights.push(chamber.len() as u64 - 1);

                break 'outer;
            }
        }
    }

    // calculate how many cycles the target number of fallen rocks is equivalent to, and what the
    // remainder is
    let n = (1000000000000 - 1 - repeat_start) / repeat_length;
    let r = (1000000000000 - 1 - repeat_start) % repeat_length;

    // the height is this number of cycles mulitplied by the height of a cycle, plus
    // the height of remainder rocks being dropped
    println!(
        "predicted height: {}",
        n * repeat_delta_h + cached_heights[(repeat_start + r) as usize]
    );
}
