// Day 17: Pyroclastic Flow
//
// Find height of list of Tetris shaped rocks after 2022 rocks have stopped falling
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
enum Jet {
    Left,
    Right,
}

impl Jet {
    fn from_char(c: char) -> Result<Self, ()> {
        match c {
            '<' => Ok(Jet::Left),
            '>' => Ok(Jet::Right),
            _ => Err(()),
        }
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day17/input"
    } else {
        "assets/day17/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut jets = Vec::<Jet>::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        for c in line.chars() {
            jets.push(Jet::from_char(c).unwrap());
        }
    }

    let mut chamber = Vec::<u8>::new();
    chamber.push(0b11111111); // adding the floor

    let mut jets = jets.iter().cycle();

    let s1 = vec![0b00111100u8];
    let s2 = vec![0b00010000u8, 0b00111000u8, 0b00010000u8];
    let s3 = vec![0b00111000u8, 0b00001000u8, 0b00001000u8];
    let s4 = vec![0b00100000u8, 0b00100000u8, 0b00100000u8, 0b00100000u8];
    let s5 = vec![0b00110000u8, 0b00110000u8];

    let shape_sequence = vec![s1, s2, s3, s4, s5];
    let mut shape_iterator = shape_sequence.iter().cycle();

    let mut num_shapes_fallen = 0;

    while num_shapes_fallen < 2022 {
        let mut next_shape = shape_iterator.next().unwrap().clone();

        'outer: for _ in 0..3 {
            match jets.next().unwrap() {
                Jet::Left => {
                    for r in next_shape.iter() {
                        if r & 0b10000000u8 != 0 {
                            continue 'outer;
                        }
                    }

                    for r in next_shape.iter_mut() {
                        *r = *r << 1;
                    }
                }
                Jet::Right => {
                    for r in next_shape.iter() {
                        if r & 0b00000010u8 != 0 {
                            continue 'outer;
                        }
                    }

                    for r in next_shape.iter_mut() {
                        *r = *r >> 1;
                    }
                }
            }
        }

        let mut shape_height = chamber.len();

        'outer: loop {
            let mut fall_complete = false;

            match jets.next().unwrap() {
                Jet::Left => {
                    let mut can_move = true;
                    for (i, r) in next_shape.iter().enumerate() {
                        if r & 0b10000000u8 != 0 {
                            can_move = false;
                        }

                        if shape_height + i < chamber.len()
                            && (*r << 1) & chamber[shape_height + i] != 0
                        {
                            can_move = false;
                        }
                    }

                    if can_move {
                        for r in next_shape.iter_mut() {
                            *r = *r << 1;
                        }
                    }
                }
                Jet::Right => {
                    let mut can_move = true;
                    for (i, r) in next_shape.iter().enumerate() {
                        if r & 0b00000010u8 != 0 {
                            can_move = false;
                        }

                        if shape_height + i < chamber.len()
                            && (*r >> 1) & chamber[shape_height + i] != 0
                        {
                            can_move = false;
                        }
                    }

                    if can_move {
                        for r in next_shape.iter_mut() {
                            *r = *r >> 1;
                        }
                    }
                }
            }

            shape_height -= 1;

            'inner: for (i, r) in next_shape.iter().enumerate() {
                if shape_height + i >= chamber.len() {
                    break 'inner;
                }
                if *r & chamber[shape_height + i] != 0 {
                    fall_complete = true;
                    break 'inner;
                }
            }

            if fall_complete {
                for (i, r) in next_shape.iter().enumerate() {
                    if shape_height + i + 1 < chamber.len() {
                        chamber[shape_height + i + 1] |= r;
                    } else {
                        chamber.push(*r);
                    }
                }
                num_shapes_fallen += 1;

                break 'outer;
            }
        }
    }

    println!(
        "after 2022 rocks have fallen, the pile is {}",
        chamber.len() - 1
    );
}
