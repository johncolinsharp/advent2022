// Day 6: Tuning Trouble
//
// Identify end of first 4 character sequence where all characters are different to one another
use std::collections::VecDeque;
use std::env;
use std::fs;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day6/input"
    } else {
        "assets/day6/input_test"
    };

    let mut signal = fs::read(file_path)
        .unwrap()
        .into_iter()
        .map(|c| c as char)
        .collect::<Vec<char>>();
    signal.pop().unwrap();
    let signal = signal;

    let mut i = 0;
    let mut clear_zones = VecDeque::<u32>::new();
    loop {
        let c = signal[i];
        let mut clear_zone = 0;

        if clear_zones.len() == 3 {
            if clear_zones[0] >= 1 && clear_zones[1] >= 2 && clear_zones[2] >= 3 {
                break;
            }

            clear_zones.pop_back().unwrap();
        }

        for j in 1..4 {
            if c == signal[i + j] {
                break;
            }
            clear_zone += 1;
        }

        clear_zones.push_front(clear_zone);

        i += 1;
    }

    println!("{} {}", i + 1, signal[i]);
}
