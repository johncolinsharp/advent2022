// Day 16: Proboscidea Volcanium (part 2)
//
// Find route to release most pressure in 26 minutes, with an elephant helping
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;
use regex::Regex;
use lazy_static::lazy_static;
use std::collections::HashMap;
use std::iter;

#[derive(Debug)]
struct Valve {
    id: String,
    flow_rate: u64,
    connections: Vec<String>,
    distances: HashMap<String, u64>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct ValveDist {
    distance: u64,
    id: String,
}

fn calc_max_pressure_released(
    current_valve_id_me: String,
    current_valve_id_elephant: String,
    valves: &HashMap::<String, Valve>,
    max_pressure_released: &mut u64, valve_ids_to_visit: Vec<String>,
    total_pressure_released: u64,
    time_remaining_me: u64,
    time_remaining_elephant: u64) {

    if total_pressure_released > *max_pressure_released {
        *max_pressure_released = total_pressure_released;
    }

    // TODO take ordered pairs from valve_ids_to_visit
    for (i, visit_valve_id) in valve_ids_to_visit.iter().enumerate() {
        let mut next_valve_ids_to_visit = valve_ids_to_visit.clone();
        next_valve_ids_to_visit.remove(i);

        for (j, visit_valve_id_elephant) in next_valve_ids_to_visit.iter().enumerate() {
            let mut next_valve_ids_to_visit = valve_ids_to_visit.clone();
            next_valve_ids_to_visit.remove(j);

            // travel time from current valve to next one we're going to visit, plus 1 minute for valve
            // opening time
            let time_to_travel_and_open_valve = valves[&current_valve_id_me].distances[visit_valve_id] + 1;
            let time_to_travel_and_open_valve_elephant = valves[&current_valve_id_elephant].distances[visit_valve_id_elephant] + 1;

            let time_remaining_me = if time_remaining_me > time_to_travel_and_open_valve {
                time_remaining_me - time_to_travel_and_open_valve
            } else {
                0
            };
            let time_remaining_elephant = if time_remaining_elephant > time_to_travel_and_open_valve_elephant {
                time_remaining_elephant - time_to_travel_and_open_valve_elephant
            } else {
                0
            };

            if time_remaining_me == 0 && time_remaining_elephant == 0 {
                continue;
            }

            let total_pressure_released = total_pressure_released + time_remaining_me*valves[visit_valve_id].flow_rate + time_remaining_elephant*valves[visit_valve_id_elephant].flow_rate;

            calc_max_pressure_released(visit_valve_id.clone(), visit_valve_id_elephant.clone(), valves,
            max_pressure_released, next_valve_ids_to_visit, total_pressure_released, time_remaining_me,
            time_remaining_elephant);
        }

    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day16/input"
    } else {
        "assets/day16/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut valves = HashMap::<String, Valve>::new();
    let mut working_valve_ids = Vec::<String>::new();

    // get list of worthwhile end points
    // work out shortest distance between each wep and shortest distance from AA to each wep
    // explore every path break when distance goes over 30
    // keep track of largest pressure release
    for (i, line) in reader.lines().map(|l| l.unwrap()).enumerate() {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Valve ([A-Z][A-Z]) has flow rate=(\d+); tunnel(?:s?) lead(?:s?) to valve(?:s?) (.*)").unwrap();
        }

        let captures = RE.captures(&line).unwrap();

        let valve_id = &captures[1];
        let flow_rate = u64::from_str(&captures[2]).unwrap();
        let connections = captures[3].split(", ").map(|c| c.to_string()).collect::<Vec<_>>();

        valves.insert(valve_id.to_string(), Valve{id: valve_id.to_string(), flow_rate, connections,
            distances: HashMap::new()});

        if flow_rate > 0 {
            working_valve_ids.push(valve_id.to_string());
        }
    }

    // work out shortest distance from origin to each working valve
    for dest_id in working_valve_ids.iter().chain(iter::once(&("AA".to_string()))) {
        let mut to_visit = Vec::<String>::new();
        for v in valves.values_mut() {
            let distance = if v.id == dest_id.to_string() {
                0 
            } else {
                u64::MAX
            };
            to_visit.push(v.id.clone());
            v.distances.insert(dest_id.to_string(), distance);
        }

        loop {
            let mut min_dist = u64::MAX;
            let mut target_i = None;
            for (i, id) in to_visit.iter().enumerate() {
                if valves[id].distances[dest_id] < min_dist {
                    min_dist = valves[id].distances[dest_id];
                    target_i = Some(i); 
                }
            }
            let visiting_id = if let Some(i) = target_i {
                to_visit.remove(i)
            } else {
                break;
            };

            let neighbours = valves[&visiting_id].connections.iter().map(|c| c.to_string()).collect::<Vec::<String>>();

            for neighbour_id in neighbours.iter() {
                let d = valves[&visiting_id].distances[dest_id] + 1;
                if d <  valves[neighbour_id].distances[dest_id] {
                    *valves.get_mut(neighbour_id).unwrap().distances.get_mut(dest_id).unwrap() = d;
                }
            }
        }
    }

    let mut max_pressure_released = 0u64;
    let mut total_pressure_released = 0u64;
    let mut valve_ids_to_visit = working_valve_ids.clone();
    let mut time_remaining = 26u64;

    calc_max_pressure_released("AA".to_string(), "AA".to_string(), &valves,
    &mut max_pressure_released, valve_ids_to_visit,
    total_pressure_released,
    time_remaining, time_remaining);

    println!("{}", max_pressure_released);
}
