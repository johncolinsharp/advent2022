// Day 7: No Space Left On Device
//
// Parse an exploration of a file system and sum sizes of all directories 
// with size < 100000
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day7/input"
    } else {
        "assets/day7/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut current_path = Vec::<String>::new();
    let mut current_size_total = 0u64;
    let mut dir_lookup = HashMap::<String, u64>::new();

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        if line.starts_with("$ cd") {
            let target = line.rsplit(' ').next().unwrap();

            for i in 0..current_path.len() {
                let canonical_path = current_path[..i+1].join("/");
                if dir_lookup.contains_key(&canonical_path) {
                    *dir_lookup.get_mut(&canonical_path).unwrap() += current_size_total;

                    if dir_lookup[&canonical_path] > 100000 {
                        dir_lookup.remove(&canonical_path);
                    }
                }
            }

            current_size_total = 0;

            if target == ".." {
                current_path.pop();
            } else {
                current_path.push(target.to_string());
            }
        } else if line.starts_with("$ ls") {
            dir_lookup.insert(current_path.join("/").to_string(), 0u64);
        } else if line.starts_with("dir") {
            // no need to do anything
        } else {
            let size = u64::from_str(line.split(' ').next().unwrap()).unwrap();
            current_size_total += size;
        }
    }

    for i in 0..current_path.len() {
        let canonical_path = current_path[..i+1].join("/");
        if dir_lookup.contains_key(&canonical_path) {
            *dir_lookup.get_mut(&canonical_path).unwrap() += current_size_total;

            if dir_lookup[&canonical_path] > 100000 {
                dir_lookup.remove(&canonical_path);
            }
        }
    }

    let mut answer = 0u64;
    for v in dir_lookup.values() {
        if *v <= 100000 {
            answer += v;
        }
    }

    println!("total of all small directories {}", answer);
}
