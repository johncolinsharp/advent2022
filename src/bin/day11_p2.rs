// Day 11: Monkey in the Middle (part 2)
//
// Calculate state of game of 'Keep Away' after 10000 rounds
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;
use std::collections::VecDeque;
use std::mem::swap;

#[derive(Debug)]
enum Operator {
    Add,
    Mul,
}

impl FromStr for Operator {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "+" => Ok(Operator::Add),
            "*" => Ok(Operator::Mul),
            _ => panic!(),
        }
    }
}

#[derive(Debug)]
enum Operand {
    Old,
    Val(u64),
}

impl FromStr for Operand {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "old" => Ok(Operand::Old),
            _ => Ok(Operand::Val(u64::from_str(s).unwrap())),
        }
    }
}

#[derive(Debug)]
struct Operation {
    a: Operand,
    b: Operand,
    o: Operator,
}

#[derive(Debug)]
struct Monkey {
    total_inspections: u64,
    starting_items: VecDeque<u64>,
    operation: Operation,
    test: (u64, usize, usize), // divisior, monkey if true, monkey if false
}

impl Monkey {
    fn new() -> Self {
        Monkey{
            total_inspections: 0,
            starting_items: VecDeque::new(),
            operation: Operation{a: Operand::Old, b: Operand::Old, o: Operator::Add},
            test: (0, 0, 0),
        }
    }
}

fn do_operation(old: u64, op: &Operation) -> u64 {
    let a = match op.a {
        Operand::Old => old,
        Operand::Val(v) => v,
    };

    let b = match op.b {
        Operand::Old => old,
        Operand::Val(v) => v,
    };

    match op.o {
        Operator::Add => a + b,
        Operator::Mul => a * b,
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day11/input"
    } else {
        "assets/day11/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut current_monkey = Monkey::new();
    let mut monkey_list = Vec::new();

    for (i, line) in reader.lines().enumerate() {
        let line = line.expect("could not read line").to_string();
        let i = i%7;
        match i {
            0 => { },
            1 => {
                let starting_items = line
                    .strip_prefix("  Starting items: ").unwrap();
                let starting_items = starting_items.split(", ")
                    .map(|item| u64::from_str(item).unwrap())
                    .collect::<VecDeque<u64>>();
                current_monkey.starting_items = starting_items;
            },
            2 => {
                let operation = line
                    .strip_prefix("  Operation: new = ")
                    .unwrap()
                    .split(" ")
                    .collect::<Vec<_>>();
                let a = Operand::from_str(operation[0]).unwrap();
                let b = Operand::from_str(operation[2]).unwrap();
                let o = Operator::from_str(operation[1]).unwrap();
                current_monkey.operation = Operation{a, b, o};
            },
            3 => {
                let test = line
                    .strip_prefix("  Test: divisible by ")
                    .unwrap();
                let test = u64::from_str(test).unwrap();
                current_monkey.test.0 = test;
            },
            4 => {
                let monkey_if_true = line
                    .strip_prefix("    If true: throw to monkey ")
                    .unwrap();
                let monkey_if_true = usize::from_str(monkey_if_true).unwrap();
                current_monkey.test.1 = monkey_if_true;
            },
            5 => {
                let monkey_if_false = line
                    .strip_prefix("    If false: throw to monkey ")
                    .unwrap();
                let monkey_if_false = usize::from_str(monkey_if_false).unwrap();
                current_monkey.test.2 = monkey_if_false;

                monkey_list.push(current_monkey);
                current_monkey = Monkey::new(); 
            }
            _ => {},
        }
    }

    let mut total_m = 1u64;
    for monkey in &monkey_list {
        total_m *= monkey.test.0;
    }

    for _round in 0..10000 {
        for i in 0..monkey_list.len() {
            let current_monkey = &mut monkey_list[i];
            let mut throws = Vec::new();
            while let Some(item) = current_monkey.starting_items.pop_front() {
                current_monkey.total_inspections += 1;
                let item = item%total_m;
                let item = do_operation(item, &current_monkey.operation);

                let target_monkey_i = if item % current_monkey.test.0 == 0 {
                    current_monkey.test.1
                } else {
                    current_monkey.test.2
                };

                throws.push((target_monkey_i, item));
            }

            for (target_monkey_i, item) in throws {
                monkey_list[target_monkey_i].starting_items.push_back(item);
            }
        }
    }

    let mut maxes = [0u64; 2];

    for monkey in monkey_list {
        let mut val = monkey.total_inspections;
        if val > maxes[0] {
            swap(&mut maxes[0], &mut val);
        }
        if val > maxes[1] {
            maxes[1] = val;
        }
    }

    println!("{}", maxes[0] * maxes[1]);
}
