// 10: Cathode-Ray Tube (part 2)
//  Run program on CPU, print image that program would produce on CRT
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day10/input"
    } else {
        "assets/day10/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut pos = 0i32;
    let mut reg_v = 1; //value in register *after* 'cycle'
    let mut next_check_point = 20;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let (v, cycles_to_do) = if let Some(v) = line.strip_prefix("addx ") {
            let v = i32::from_str(v).unwrap();
            (v, 2)
        } else {
            (0, 1)
        };

        for c in 0..cycles_to_do {
            let show = if pos.abs_diff(reg_v) < 2 { true } else { false };
            print!("{}", if show {"#" } else { "." }); 

            pos += 1;
            if pos == 40 {
                pos = 0;
                println!("");
            }
        }

        reg_v += v;
    }
}
