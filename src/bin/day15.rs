// Day 15: Beacon Exclusion Zone
// For a given row of a map, find the number of positions where
// a beacon cannot be present
use lazy_static::lazy_static;
use regex::Regex;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::BTreeSet;
use std::str::FromStr;
use std::cmp::Ordering;
// use std::collections::HashSet;

#[derive(PartialEq, Eq, Clone, Debug)]
struct LinePoint {
    p: i64,
    start: bool,
}

impl Ord for LinePoint {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.p == other.p {
            if self.start == other.start {
                return Ordering::Equal;
            }

            if self.start == false {
                return Ordering::Greater;
            }

            return Ordering::Less;
        }

        return self.p.cmp(&other.p);
    }
}

impl PartialOrd for LinePoint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct LineSet {
    line_points: BTreeSet<LinePoint>,
    len: u64,
}

impl LineSet {
    fn new() -> Self {
        LineSet { line_points: BTreeSet::new(), len: 0 }
    }

    fn insert(&mut self, segment: [i64; 2]) {
        // don't bother adding 0 length segments
        if segment[0] == segment[1] {
            return;
        }

        let start_point = LinePoint{p: segment[0], start: true};
        let end_point = LinePoint{p: segment[1], start: false};

        // add line segment length to total length, will subtract double
        // counted sections in what follows
        self.len += (end_point.p - start_point.p) as u64;

        let mut mid = self.line_points.split_off(&start_point);
        let mut end = mid.split_off(&end_point);

        let mut prev = &start_point;
        // loop through encompassed points, subtracting segments that
        // have been double counted
        for p in mid.iter() {
            if p.start {
                prev = p;
            } else {
                self.len -= (p.p - prev.p) as u64;
            }
        }

        if let Some(l) = mid.last() {
            // if mid points end with an incomplete line, subtract
            // the double counted portion from the start of this line
            // to the end of the segment just added
            if l.start {
                self.len -= (end_point.p - l.p) as u64;
            }
        } else if let Some(l) = self.line_points.last() {
            // if no midpoints, and the start section ends with
            // a line starting, then the line segment just added is
            // completely encompassed, subtract its entire length
            // since double counted
            if l.start {
                self.len -= (end_point.p - start_point.p) as u64;
            }
        }

        if let Some(l) = self.line_points.last() {
            // if added segment added in vacant section, can add
            // segment's start point to start section
            if !l.start {
                if l.p == start_point.p {
                    self.line_points.pop_last().unwrap();
                } else {
                    self.line_points.insert(start_point);
                }
            }
        } else {
            // if added segment starts after all previous segments,
            // can just add start point on end
            self.line_points.insert(start_point);
        }

        if let Some(f) = end.first() {
            // if end segment begins with a line starting, can
            // just add added segments end point to end section
            if f.start {
                if f.p == end_point.p {
                    end.pop_first().unwrap();
                } else {
                    end.insert(end_point);
                }
            }
        } else {
            // if added segment ends after all previous segments,
            // can just add end point on end
            end.insert(end_point);
        }

        // append start and end, and so remove middle
        self.line_points.append(&mut end);
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day15/input"
    } else {
        "assets/day15/input_test"
    };

    let target_y = if env::var("ADVENT_TEST").is_err() {
        2000000
    } else {
        10
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);
    let mut excluded_positions = LineSet::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Sensor at x=(\-?\d+), y=(\-?\d+): closest beacon is at x=(\-?\d+), y=(\-?\d+)").unwrap();
        }
        let captures = RE.captures(&line).unwrap();

        let sensor_loc = [i64::from_str(&captures[1]).unwrap(),
            i64::from_str(&captures[2]).unwrap()];
        let beacon_loc = [i64::from_str(&captures[3]).unwrap(),
            i64::from_str(&captures[4]).unwrap()];

        let manhattan_d = sensor_loc[0].abs_diff(beacon_loc[0]) + sensor_loc[1].abs_diff(beacon_loc[1]);

        if sensor_loc[1].abs_diff(target_y) < manhattan_d {
            let x_lim = manhattan_d.abs_diff(sensor_loc[1].abs_diff(target_y)) as i64;

            if target_y == beacon_loc[1] {
                excluded_positions.insert([sensor_loc[0]-x_lim, beacon_loc[0]]);
                excluded_positions.insert([beacon_loc[0]+1, sensor_loc[0]+x_lim+1]);
            } else {
                excluded_positions.insert([sensor_loc[0]-x_lim, sensor_loc[0]+x_lim+1]);
            }
        }
    }

    println!("excluded positions: {}", excluded_positions.len);
}
