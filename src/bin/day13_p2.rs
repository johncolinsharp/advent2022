// Day 13: Distress Signal (part 2)
// Find how many packets are less than [[2]] and [[6]]
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter;

#[derive(Debug)]
enum Member {
    Int(u32),
    List(List),
}

#[derive(Debug)]
struct List {
    members: Vec<Member>,
}

fn parse_packet(line: &str) -> Member {
    let mut incomplete_parses = Vec::<Member>::new();
    
    for c in line.chars() {
        if c == '[' {
            // 
            incomplete_parses.push(Member::List(List{members: Vec::new()}));
        } else if c == ']' {
            if incomplete_parses.len() == 1 {
                break;
            }

            if let Some(Member::Int(_)) = incomplete_parses.last() {
                let new_m = incomplete_parses.pop().unwrap();

                if let Some(&mut Member::List(ref mut l)) = incomplete_parses.last_mut() {
                    l.members.push(new_m);
                }
                else
                {
                    panic!();
                }
            }

            if incomplete_parses.len() == 1 {
                break;
            }

            // add to previous list
            let new_m = incomplete_parses.pop().unwrap();

            if let Some(&mut Member::List(ref mut l)) = incomplete_parses.last_mut() {
                l.members.push(new_m);
            } else {
                panic!();
            }
        } else if c== ',' {
            // lists will already be completed, so only need to add numbers here
            if let Some(Member::Int(_)) = incomplete_parses.last() {
            } else {
                continue;
            }

            let new_m = incomplete_parses.pop().unwrap();

            if let Some(&mut Member::List(ref mut l)) = incomplete_parses.last_mut() {
                l.members.push(new_m);
            }else {
                panic!();
            }

        } else if c.is_numeric() {
            if let Some(&Member::Int(v)) = incomplete_parses.last() {
                *(incomplete_parses.last_mut().unwrap()) = Member::Int(10*v + c.to_digit(10).unwrap());
            } else {
                incomplete_parses.push(Member::Int(c.to_digit(10).unwrap()));
            }
        }
    }

    if incomplete_parses.len() > 1 {
        panic!();
    }

    return incomplete_parses.pop().unwrap();
}

fn less_than(left: &Member, right: &Member) -> Option<bool> {
    match left {
        Member::Int(left_v) => {
            match right {
                Member::Int(right_v) => {
                    if left_v < right_v {
                        return Some(true);
                    } else if left_v > right_v {
                        return Some(false);
                    }
                    return None;
                },
                Member::List(_right_l) => {
                    return less_than(&Member::List(List{members: vec!(
                                Member::Int(*left_v))}), right);
                },
            }
        },
        Member::List(ref l) => {
            match right {
                Member::Int(right_v) => {
                    return less_than(left, 
                        &Member::List(List{members: vec!(Member::Int(*right_v))}));
                },
                Member::List(ref r) => {
                    for (m_l, m_r) in l.members.iter().zip(r.members.iter()) {
                        if let Some(b) = less_than(m_l, m_r) {
                            return Some(b);
                        }
                    }

                    if l.members.len() < r.members.len() {
                        return Some(true);
                    }
                    if l.members.len() > r.members.len() {
                        return Some(false);
                    }

                    return None;
                },
            }
        },
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day13/input"
    } else {
        "assets/day13/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut packet_l = Member::Int(0);
    let mut packet_r = Member::Int(0);

    let mut less_than_ref_1 = 1;
    let mut less_than_ref_2 = 2;
    let ref_1 = parse_packet("[[2]]");
    let ref_2 = parse_packet("[[6]]");

    for (i, line) in reader.lines().chain(iter::once(Ok("".to_string()))).enumerate() {
        let line = line.expect("could not read line").to_string();

        if i%3 == 0 {
            packet_l = parse_packet(&line);
        }
        if i%3 == 1 {
            packet_r = parse_packet(&line);
        }
        if i%3 == 2 {
            if let Some(true) = less_than(&packet_l, &ref_1) {
                less_than_ref_1 += 1;
            }
            if let Some(true) = less_than(&packet_r, &ref_1) {
                less_than_ref_1 += 1;
            }

            if let Some(true) = less_than(&packet_l, &ref_2) {
                less_than_ref_2 += 1;
            }
            if let Some(true) = less_than(&packet_r, &ref_2) {
                less_than_ref_2 += 1;
            }
        }
    }

    println!("answer is {}", less_than_ref_1 * less_than_ref_2);
}
