// 10: Cathode-Ray Tube
//  Run program on CPU, calculate value based on value in register at each cycle
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day10/input"
    } else {
        "assets/day10/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut cycle = 1;
    let mut reg_v = 1; //value in register *after* 'cycle'
    let mut next_check_point = 20;
    let mut signal_strength = 0;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let v = if let Some(v) = line.strip_prefix("addx ") {
            let v = i32::from_str(v).unwrap();
            cycle += 2;
            v
        } else {
            cycle += 1;
            0
        };

        if cycle > next_check_point {
            println!("during checkpoint {} value was {}",
                     next_check_point, reg_v);
            signal_strength += next_check_point * reg_v;
            next_check_point += 40;
        }

        reg_v += v;
    }

    println!("signal strength {}", signal_strength);
}
