// Day 1: Calorie Counting
//
// Find which elf is carrying the most calories
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day1/input"
    } else {
        "assets/day1/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut current_elf_calories = 0u32;
    let mut max_elf_calories = 0u32;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        if line == "" {
            if current_elf_calories > max_elf_calories {
                max_elf_calories = current_elf_calories;
            }
            current_elf_calories = 0;
        } else {
            current_elf_calories += u32::from_str(&line).unwrap();
        }
    }

    if current_elf_calories > max_elf_calories {
        max_elf_calories = current_elf_calories;
    }

    println!("max elf calories {}", max_elf_calories);
}
