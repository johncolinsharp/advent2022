// Day 2: Rock Paper Scissors
//
// Find the total score playing rock paper scissors with
// (A, B, C) = rock, paper, scissors = (X, Y, Z)
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

const ROCK: u32 = 1;
const PAPER: u32 = 2;
const SCISSORS: u32 = 3;

fn get_rps_res(opponent_play: u32, your_play: u32) -> u32 {
    if opponent_play == your_play {
        //draw
        return your_play + 3;
    } else if (your_play > opponent_play && (your_play - opponent_play) % 2 == 1)
        || (opponent_play > your_play && (opponent_play - your_play) % 2 == 0)
    {
        // your win
        return your_play + 6;
    }
    // your loss
    your_play
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day2/input"
    } else {
        "assets/day2/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let lookup_opponent = HashMap::from([("A", ROCK), ("B", PAPER), ("C", SCISSORS)]);
    let lookup_you = HashMap::from([("X", ROCK), ("Y", PAPER), ("Z", SCISSORS)]);

    let mut total_score = 0u32;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let line: Vec<&str> = line.split(' ').collect();

        total_score += get_rps_res(lookup_opponent[line[0]], lookup_you[line[1]]);
    }

    println!("total score is {}", total_score);
}
