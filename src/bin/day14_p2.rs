// Day 14: Regolith Reservoir (part 2)
// Compute the paths of falling grains of sand and count how many come to 
// rest in a maze with a floor
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::HashSet;
use std::cmp;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day14/input"
    } else {
        "assets/day14/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);
    let mut map = HashSet::<[u32; 2]>::new();
    let mut low_limit = 0;

    for line in reader.lines().map(|l| l.unwrap()) {
        let mut prev_point: Option<[u32; 2]> = None;
        for path in line.split(" -> ") {
            let new_point = path.split(",")
                .map(|p| u32::from_str(p).unwrap())
                .collect::<Vec<u32>>();
            let new_point = [new_point[0], new_point[1]];

            if let Some(prev_point) = prev_point {
                let x_min = cmp::min(prev_point[0], new_point[0]);
                let x_max = cmp::max(prev_point[0], new_point[0]);
                let y_min = cmp::min(prev_point[1], new_point[1]);
                let y_max = cmp::max(prev_point[1], new_point[1]);
                if y_max > low_limit {
                    low_limit = y_max;
                }
                for x in x_min..=x_max {
                    for y in y_min..=y_max {
                        map.insert([x, y]);
                    }
                }
            }

            prev_point = Some(new_point);
        }
    }

    let floor_loc = low_limit + 2;
    let mut grain_count = 0;

    'outer: loop {
        let mut sand_loc = [500, 0]; 
        loop {
            if sand_loc[1] < floor_loc-1 {
                // try move down
                if !map.contains(&[sand_loc[0], sand_loc[1]+1]) {
                    sand_loc[1] += 1;
                    continue;
                }
                // try move diagonal left
                if !map.contains(&[sand_loc[0]-1, sand_loc[1]+1]) {
                    sand_loc[0] -= 1;
                    sand_loc[1] += 1;
                    continue;
                }
                // try move diagonal right
                if !map.contains(&[sand_loc[0]+1, sand_loc[1]+1]) {
                    sand_loc[0] += 1;
                    sand_loc[1] += 1;
                    continue;
                }
            }

            if map.contains(&[500, 0]) {
                break 'outer;
            }

            // sand can't move anywhere
            map.insert([sand_loc[0], sand_loc[1]]);
            grain_count += 1;

            break;
        }
    }

    println!("{} grains of sand fell and came to rest", grain_count);
}

