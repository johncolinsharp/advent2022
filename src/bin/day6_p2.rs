// Day 6: Tuning Trouble (part 2)
//
// Identify end of first 14 character sequence where all characters are different to one another
use std::collections::VecDeque;
use std::env;
use std::fs;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day6/input"
    } else {
        "assets/day6/input_test"
    };

    let mut signal = fs::read(file_path)
        .unwrap()
        .into_iter()
        .map(|c| c as char)
        .collect::<Vec<char>>();
    signal.pop().unwrap();
    let signal = signal;

    let mut i = 0;
    // will keep track of the last 13 'clear_zones', how far the character at that position can
    // look ahead without encountering a duplicate
    let mut clear_zones = VecDeque::<u32>::new();

    loop {
        let c = signal[i];
        let mut clear_zone = 0;

        // check previous clear_zones to see if this character is
        // at the end of a sequence where all characters are different
        // to one another
        if clear_zones.len() == 13 {
            let mut success = true;

            for (i, z) in clear_zones.iter().enumerate() {
                if *z < (i as u32) + 1 {
                    success = false;
                    break;
                }
            }
            if success {
                break;
            }

            clear_zones.pop_back().unwrap();
        }

        // calculate the clear_zone for this character
        for j in 1..14 {
            if i + j > signal.len() - 1 {
                break;
            }

            if c == signal[i + j] {
                break;
            }

            clear_zone += 1;
        }

        clear_zones.push_front(clear_zone);

        i += 1;
    }

    println!("{} {}", i + 1, signal[i]);
}
