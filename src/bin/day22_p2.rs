// Day 22: Monkey Map (part 2)
// Find end point of route across cube
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(PartialEq, Eq, Debug)]
enum Tile {
    Blank,
    Open,
    Wall,
}

#[derive(Debug)]
enum Instruction {
    Forward(u32),
    TurnLeft,
    TurnRight,
}

fn parse_instruction_list(s: String) -> Vec<Instruction> {
    let mut instruction_list = Vec::<Instruction>::new();

    let mut parsing_num = false;
    let mut acc = 0;

    for c in s.chars() {
        if let Some(v) = c.to_digit(10) {
            parsing_num = true;
            acc = 10 * acc + v;
        } else {
            if parsing_num {
                instruction_list.push(Instruction::Forward(acc));
                parsing_num = false;
                acc = 0;
            }

            instruction_list.push(match c {
                'L' => Instruction::TurnLeft,
                'R' => Instruction::TurnRight,
                _ => panic!(),
            });
        }
    }

    if parsing_num {
        instruction_list.push(Instruction::Forward(acc));
    }

    instruction_list
}

fn advance(mut y: u32, mut x: u32, direction: u32, side_len: u32) -> (u32, u32, bool) {
    match direction {
        0 => {
            // right
            x = (x + 1) % side_len;
            let left_patch = x == 0;

            (y, x, left_patch)
        }
        1 => {
            // down
            y = (y + 1) % side_len;
            let left_patch = y == 0;

            (y, x, left_patch)
        }
        2 => {
            // left
            x = (x + side_len - 1) % side_len;
            let left_patch = x == side_len - 1;

            (y, x, left_patch)
        }
        3 => {
            // up
            y = (y + side_len - 1) % side_len;
            let left_patch = y == side_len - 1;

            (y, x, left_patch)
        }
        _ => {
            panic!();
        }
    }
}

fn get_connection(
    patch_x: u32,
    patch_y: u32,
    direction: u32,
    connections: &HashMap<(u32, u32, u32), (u32, u32, u32)>,
) -> (u32, u32, u32) {
    connections[&(patch_x, patch_y, direction)]
}

fn step_forward(
    steps: u32,
    mut x: usize,
    mut y: usize,
    mut direction: u32,
    map: &[Vec<Tile>],
    side_len: u32,
    connections: &HashMap<(u32, u32, u32), (u32, u32, u32)>,
) -> (usize, usize, u32) {
    for _ in 0..steps {
        let mut patch_x = x as u32 / side_len;
        let mut patch_y = y as u32 / side_len;

        let x_p = x as u32 % side_len;
        let y_p = y as u32 % side_len;

        let (mut y_p, mut x_p, left_patch) = advance(y_p, x_p, direction, side_len);

        let mut new_direction = direction;
        if left_patch {
            // get new patch and direction, rotate x_p, y_p until correct
            let (new_patch_x, new_patch_y, target_direction) =
                get_connection(patch_x, patch_y, direction, connections);
            patch_x = new_patch_x;
            patch_y = new_patch_y;

            loop {
                if new_direction == target_direction {
                    break;
                }
                let y_p_old = y_p;
                y_p = x_p;
                x_p = side_len - 1 - y_p_old;
                new_direction = (new_direction + 1) % 4;
            }
        }

        let x_new = (patch_x * side_len + x_p) as usize;
        let y_new = (patch_y * side_len + y_p) as usize;

        if map[y_new][x_new] == Tile::Wall {
            break;
        }

        x = x_new;
        y = y_new;
        direction = new_direction;
    }

    (x, y, direction)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // read into vector of vector of enums (blank, tile, wall)
    let f = if env::var("ADVENT_TEST").is_ok() {
        File::open("assets/day22/input_test")?
    } else {
        File::open("assets/day22/input")?
    };
    let f = BufReader::new(f);
    let mut lines = f.lines().map(|l| l.unwrap());
    let side_len = if env::var("ADVENT_TEST").is_ok() {
        4
    } else {
        50
    };

    let connections = if env::var("ADVENT_TEST").is_ok() {
        HashMap::from([
            ((2, 0, 0), (3, 2, 2)),
            ((2, 0, 1), (2, 1, 1)),
            ((2, 0, 2), (1, 1, 1)),
            ((2, 0, 3), (0, 1, 1)),
            ((0, 1, 0), (1, 1, 0)),
            ((0, 1, 1), (2, 2, 3)),
            ((0, 1, 2), (3, 2, 3)),
            ((0, 1, 3), (2, 0, 1)),
            ((1, 1, 0), (2, 1, 0)),
            ((1, 1, 1), (2, 2, 0)),
            ((1, 1, 2), (0, 1, 2)),
            ((1, 1, 3), (2, 0, 0)),
            ((2, 1, 0), (3, 2, 1)),
            ((2, 1, 1), (2, 2, 1)),
            ((2, 1, 2), (1, 1, 2)),
            ((2, 1, 3), (2, 0, 3)),
            ((2, 2, 0), (3, 2, 0)),
            ((2, 2, 1), (0, 1, 3)),
            ((2, 2, 2), (1, 1, 3)),
            ((2, 2, 3), (2, 1, 3)),
            ((3, 2, 0), (2, 0, 2)),
            ((3, 2, 1), (0, 1, 0)),
            ((3, 2, 2), (2, 2, 2)),
            ((3, 2, 3), (2, 1, 2)),
        ])
    } else {
        HashMap::from([
            ((1, 0, 0), (2, 0, 0)),
            ((1, 0, 1), (1, 1, 1)),
            ((1, 0, 2), (0, 2, 0)),
            ((1, 0, 3), (0, 3, 0)),
            ((2, 0, 0), (1, 2, 2)),
            ((2, 0, 1), (1, 1, 2)),
            ((2, 0, 2), (1, 0, 2)),
            ((2, 0, 3), (0, 3, 3)),
            ((1, 1, 0), (2, 0, 3)),
            ((1, 1, 1), (1, 2, 1)),
            ((1, 1, 2), (0, 2, 1)),
            ((1, 1, 3), (1, 0, 3)),
            ((0, 2, 0), (1, 2, 0)),
            ((0, 2, 1), (0, 3, 1)),
            ((0, 2, 2), (1, 0, 0)),
            ((0, 2, 3), (1, 1, 0)),
            ((1, 2, 0), (2, 0, 2)),
            ((1, 2, 1), (0, 3, 2)),
            ((1, 2, 2), (0, 2, 2)),
            ((1, 2, 3), (1, 1, 3)),
            ((0, 3, 0), (1, 2, 3)),
            ((0, 3, 1), (2, 0, 1)),
            ((0, 3, 2), (1, 0, 1)),
            ((0, 3, 3), (0, 2, 3)),
        ])
    };

    let mut map = Vec::<Vec<Tile>>::new();

    for line in lines.by_ref() {
        if line.is_empty() {
            break;
        }

        map.push(
            line.chars()
                .map(|c| match c {
                    ' ' => Tile::Blank,
                    '.' => Tile::Open,
                    '#' => Tile::Wall,
                    _ => {
                        panic!();
                    }
                })
                .collect::<Vec<Tile>>(),
        );
    }

    let mut direction = 0;
    let mut y = 0;
    let mut x = 0;
    for t in map[0].iter() {
        if t == &Tile::Open {
            break;
        }

        x += 1;
    }

    let instruction_list = parse_instruction_list(lines.next().unwrap());

    for instruction in instruction_list {
        match instruction {
            Instruction::Forward(s) => {
                (x, y, direction) = step_forward(s, x, y, direction, &map, side_len, &connections);
            }
            Instruction::TurnLeft => {
                direction = ((direction + 4) - 1) % 4;
            }
            Instruction::TurnRight => {
                direction = ((direction + 4) + 1) % 4;
            }
        }
    }

    println!(
        "{} {} {} ... {}",
        x + 1,
        y + 1,
        direction,
        4 * (x as u32 + 1) + 1000 * (y as u32 + 1) + direction
    );

    Ok(())
}
