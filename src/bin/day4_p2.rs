// Day 4: Camp Cleanup (part 2)
//
// Find number of pairs where one encompasses the other at all
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day4/input"
    } else {
        "assets/day4/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut encompass_total = 0u32;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let a = line
            .split(",")
            .map(|x| x.split("-"))
            .flatten()
            .map(|x| u32::from_str(x).unwrap())
            .collect::<Vec<_>>();

        if a[1] < a[2] || a[3] < a[0] {
            // don't overlap
        } else {
            encompass_total += 1;
        }
    }

    println!("total: {}", encompass_total);
}
