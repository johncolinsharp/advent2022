// Day 18: Boiling Boulders
//
// Find surface area of connected cubes
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::HashSet;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day18/input"
    } else {
        "assets/day18/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut cubes = HashSet::<[i32; 3]>::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        let coords = line.split(",").map(|x| i32::from_str(x).unwrap())
                                         .collect::<Vec<i32>>();

        let coords = [coords[0], coords[1], coords[2]];
        cubes.insert(coords);
    }

    let mut total_sides = 0;

    for cube in &cubes {
        let mut sides = 6;

        for index in 0..3 {
            for v in [1, -1] {
                let mut cube = cube.clone();
                cube[index] += v;

                if cubes.contains(&cube) {
                    sides -= 1;
                }
            }
        }
        total_sides += sides;
    }

    println!("total surface area {}", total_sides);
}
