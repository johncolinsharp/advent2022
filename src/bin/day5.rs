// Day 5: Supply Stacks
//
// Manipulate a number of stacks and see what is on top of them after
// all the operations
use lazy_static::lazy_static;
use regex::Regex;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day5/input"
    } else {
        "assets/day5/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);
    let mut lines = reader.lines();

    let mut stacks = Vec::<Vec<char>>::new();

    let line = lines
        .next()
        .unwrap()
        .expect("could not read line")
        .to_string();

    for (i, c) in line.chars().enumerate() {
        if i % 4 == 1 {
            let mut new_stack = Vec::<char>::new();
            if c != ' ' {
                new_stack.push(c);
            }
            stacks.push(new_stack);
        }
    }

    while let Some(line) = lines.next() {
        let line = line.expect("could not read line").to_string();
        if line.as_bytes()[0] as char != '[' {
            break;
        }

        for (i, c) in line.chars().enumerate() {
            if i % 4 == 1 {
                let stack = &mut stacks[i / 4];
                if c != ' ' {
                    stack.push(c);
                }
            }
        }
    }

    for stack in stacks.iter_mut() {
        stack.reverse();
    }

    let lines = lines.skip(1);

    for line in lines {
        let line = line.expect("could not read line").to_string();
        lazy_static! {
            static ref RE: Regex = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();
        }
        let captures = RE.captures(&line).unwrap();
        let num_to_move = u32::from_str(&captures[1]).unwrap();
        let src_stack_i = usize::from_str(&captures[2]).unwrap() - 1;
        let dst_stack_i = usize::from_str(&captures[3]).unwrap() - 1;

        for _i in 0..num_to_move {
            let c = stacks[src_stack_i].pop().unwrap();
            stacks[dst_stack_i].push(c);
        }
    }

    for s in stacks.iter_mut() {
        print!("{}", s.pop().unwrap());
    }
    println!("");
}
