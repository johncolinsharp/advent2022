// Day 23: Unstable Diffusion
// Calculate the diffusion of elves and see what area they cover after 10 steps
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug, Eq, Hash, PartialEq)]
enum Direction {
    N,
    S,
    E,
    W,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let f = if env::var("ADVENT_TEST").is_ok() {
        File::open("assets/day23/input_test")?
    } else {
        File::open("assets/day23/input")?
    };
    let f = BufReader::new(f);

    let mut elf_list = Vec::<(i32, i32)>::new();
    let mut elf_map = HashMap::<(i32, i32), (bool, HashSet<Direction>)>::new();
    let mut directions = VecDeque::from([Direction::N, Direction::S, Direction::W, Direction::E]);

    for (y, l) in f.lines().map(|l| l.unwrap()).enumerate() {
        for (x, c) in l.chars().enumerate() {
            match c {
                '#' => {
                    elf_list.push((x as i32, y as i32));
                    elf_map.insert(
                        (x as i32, y as i32),
                        (
                            false,
                            HashSet::from([Direction::N, Direction::S, Direction::W, Direction::E]),
                        ),
                    );
                }
                '.' => (),
                _ => panic!(),
            }
        }
    }

    for _ in 0..10 {
        for elf in &elf_list {
            let mut south_possible = true;
            let mut east_possible = true;

            for a in -1..=1 {
                if let Some(neighbour) = elf_map.get_mut(&(elf.0 + a, elf.1 + 1)) {
                    neighbour.0 = true;
                    neighbour.1.remove(&Direction::N);
                    south_possible = false;
                }

                if let Some(neighbour) = elf_map.get_mut(&(elf.0 + 1, elf.1 + a)) {
                    neighbour.0 = true;
                    neighbour.1.remove(&Direction::W);
                    east_possible = false;
                }
            }

            if !south_possible {
                let e = elf_map.get_mut(&(elf.0, elf.1)).unwrap();
                e.0 = true;
                e.1.remove(&Direction::S);
            }

            if !east_possible {
                let e = elf_map.get_mut(&(elf.0, elf.1)).unwrap();
                e.0 = true;
                e.1.remove(&Direction::E);
            }
        }

        // go through map, make new map (target) -> (vec of sources)
        let mut proposed_locations = HashMap::<(i32, i32), Vec<(i32, i32)>>::new();
        for (elf, v) in &elf_map {
            if !v.0 {
                proposed_locations
                    .entry(*elf)
                    .and_modify(|v| v.push(*elf))
                    .or_insert_with(|| vec![*elf]);
            } else if v.1.is_empty() {
                proposed_locations
                    .entry(*elf)
                    .and_modify(|v| v.push(*elf))
                    .or_insert_with(|| vec![*elf]);
            } else {
                for d in &directions {
                    if v.1.contains(d) {
                        let new_location = match d {
                            Direction::N => (elf.0, elf.1 - 1),
                            Direction::E => (elf.0 + 1, elf.1),
                            Direction::S => (elf.0, elf.1 + 1),
                            Direction::W => (elf.0 - 1, elf.1),
                        };
                        proposed_locations
                            .entry(new_location)
                            .and_modify(|v| v.push(*elf))
                            .or_insert_with(|| vec![*elf]);
                        break;
                    }
                }
            }
        }

        // go through new map, if vec sources len 1, then add target else just add each of sources
        elf_list.clear();
        elf_map.clear();
        for (target, src) in proposed_locations {
            if src.len() == 1 {
                elf_list.push(target);
                elf_map.insert(
                    target,
                    (
                        false,
                        HashSet::from([Direction::N, Direction::S, Direction::E, Direction::W]),
                    ),
                );
            } else {
                for s in src {
                    elf_list.push(s);
                    elf_map.insert(
                        s,
                        (
                            false,
                            HashSet::from([Direction::N, Direction::S, Direction::E, Direction::W]),
                        ),
                    );
                }
            }
        }

        let t = directions.pop_front().unwrap();
        directions.push_back(t);
    }

    let (mut min_x, mut max_x, mut min_y, mut max_y) = (i32::MAX, i32::MIN, i32::MAX, i32::MIN);

    let num_elves = elf_list.len() as i32;
    for e in elf_map.keys() {
        if e.0 < min_x {
            min_x = e.0;
        }
        if e.1 < min_y {
            min_y = e.1;
        }
        if e.0 > max_x {
            max_x = e.0;
        }
        if e.1 > max_y {
            max_y = e.1;
        }
    }

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if elf_map.contains_key(&(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }

    println!(
        "num empty squares: {} {} {} {} {}",
        max_x,
        min_x,
        max_y,
        min_y,
        (max_x - min_x + 1) * (max_y - min_y + 1) - num_elves
    );

    Ok(())
}
