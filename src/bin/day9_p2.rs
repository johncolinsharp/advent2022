// Day 9: Rope Bridge (part 2)
//
// Find how many grid points the tail of a rope, length 9) being dragged touches
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;
use std::collections::HashSet;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day9/input"
    } else {
        "assets/day9/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut visited = HashSet::new();

    let mut rope_state = [[0i32, 0i32]; 10];
    let mut tail_location = [0i32, 0i32];
    visited.insert(tail_location.clone());

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let line = line.split(" ").collect::<Vec::<&str>>();
        let m = match line[0] {
            "U" => [0, -1],
            "L" => [-1, 0],
            "D" => [0, 1],
            "R" => [1, 0],
            _ => panic!(),
        };
        let n = u32::from_str(line[1]).unwrap();

        for _i in 0..n {
            rope_state[0] = [m[0] + rope_state[0][0], m[1] + rope_state[0][1]];

            for j in 1..10 {
                let mut d = rope_state[j-1].clone();
                if rope_state[j-1][0].abs() == 2 {
                    d[0] = rope_state[j-1][0]/2;
                }
                if rope_state[j-1][1].abs() == 2 {
                    d[1] = rope_state[j-1][1]/2;
                }
                if rope_state[j-1][0].abs() == 2 || rope_state[j-1][1].abs() == 2 {
                    rope_state[j] = [rope_state[j][0] + d[0], rope_state[j][1] + d[1]];
                    rope_state[j-1] = [rope_state[j-1][0] - d[0], rope_state[j-1][1] - d[1]];

                    if j == 9 {
                        tail_location = [tail_location[0] + d[0], tail_location[1] + d[1]];
                        visited.insert(tail_location.clone());
                    }
                }
            }
        }
    }

    println!("{}", visited.len());
}

