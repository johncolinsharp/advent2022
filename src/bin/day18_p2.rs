// Day 18: Boiling Boulders (part 2)
//
// Find exterior surface area of connected cubes
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::HashSet;
use std::collections::HashMap;
use std::str::FromStr;

fn find_external(limits_max: &[i32; 3], cubes: &HashSet::<[i32; 3]>) -> HashSet::<[i32; 3]> {
    // calculates every point that is external to shape of limits limits_max 
    // Does this by calculating path from each point to origin
    let mut distances = HashMap::<[i32; 3], u32>::new();

    for x in -1..=limits_max[0]+1 {
        for y in -1..=limits_max[1]+1 {
            for z in -1..=limits_max[2]+1 {
                distances.insert([x,y,z], u32::MAX);
            }
        }
    }
    distances.insert([0, 0, 0], 0);
    let mut external_points = HashSet::<[i32; 3]>::new();

    loop {
        let mut min = [0; 3];
        let mut dist_min = u32::MAX;
        for (k, v) in &distances {
            if v < &dist_min {
                min = [k[0], k[1], k[2]];
                dist_min = *v;
            }

        }

        if dist_min == u32::MAX {
            break;
        }
        external_points.insert([min[0],min[1],min[2]]);
        distances.remove(&[min[0],min[1],min[2]]);

        for i in 0..3 {
            for v in [-1, 1] {
                if min[i] + v >= -1 && min[i] + v <= limits_max[i]+1 {
                    // update distances
                    let mut min = min.clone();
                    min[i] = min[i] + v;
                    if cubes.contains(&min) {
                        continue;
                    }
                    if distances.contains_key(&[min[0], min[1], min[2]]) && distances[&[min[0], min[1], min[2]]] > dist_min + 1 {
                        distances.insert([min[0], min[1], min[2]], dist_min + 1);
                    }
                }
            }
        }
    }

    external_points
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day18/input"
    } else {
        "assets/day18/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut cubes = HashSet::<[i32; 3]>::new();
    let mut limits_max = [i32::MIN; 3];

    for line in reader.lines().map(|l| l.unwrap()) {
        let coords = line.split(",").map(|x| i32::from_str(x).unwrap())
                                         .collect::<Vec<i32>>();

        let coords = [coords[0], coords[1], coords[2]];

        for i in 0..3 {
            if coords[i] > limits_max[i] {
                limits_max[i] = coords[i];
            }
        }

        cubes.insert(coords);
    }

    let mut total_sides = 0;

    let external_points = find_external(&limits_max, &cubes);
    for cube in &cubes {
        let mut sides = 6;

        for index in 0..3 {
            for v in [1, -1] {
                let mut cube = cube.clone();
                 cube[index] += v;

                 if cubes.contains(&cube) {
                     sides -= 1;
                 } else if !external_points.contains(&cube) {
                     sides -= 1;
                 }
            }
        }

        total_sides += sides;
    }

    println!("total surface area {}", total_sides);
}
