// Day 7: No Space Left On Device (part 2)
//
// Find the smallest directory that will make the free space on the
// file system (size 70000000) at least 30000000
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day7/input"
    } else {
        "assets/day7/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut current_path = Vec::<String>::new();
    let mut current_size_total = 0u64;
    let mut dir_lookup = HashMap::<String, u64>::new();

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        if line.starts_with("$ cd") {
            let target = line.rsplit(' ').next().unwrap();

            for i in 0..current_path.len() {
                let canonical_path = current_path[..i+1].join("/");
                if dir_lookup.contains_key(&canonical_path) {
                    *dir_lookup.get_mut(&canonical_path).unwrap() += current_size_total;
                }
            }

            current_size_total = 0;

            if target == ".." {
                current_path.pop();
            } else {
                current_path.push(target.to_string());
            }
        } else if line.starts_with("$ ls") {
            dir_lookup.insert(current_path.join("/").to_string(), 0u64);
        } else if line.starts_with("dir") {
            // no need to do anything
        } else {
            let size = u64::from_str(line.split(' ').next().unwrap()).unwrap();
            current_size_total += size;
        }
    }

    for i in 0..current_path.len() {
        let canonical_path = current_path[..i+1].join("/");
        if dir_lookup.contains_key(&canonical_path) {
            *dir_lookup.get_mut(&canonical_path).unwrap() += current_size_total;
        }
    }

    let mut answer = 0u64;
    for v in dir_lookup.values() {
        if *v <= 100000 {
            answer += v;
        }
    }

    let total_space = 70_000_000;
    let target_space = 30_000_000;
    let free_space = total_space - dir_lookup["/"];
    let min_space_to_free = target_space - free_space;

    let mut dir_to_remove = "/".to_string();
    let mut dir_to_remove_size = dir_lookup["/"];

    for (k, v) in &dir_lookup {
        if *v >= min_space_to_free && *v < dir_to_remove_size {
            dir_to_remove_size = *v;
            dir_to_remove = k.to_string();
        }
    }

    println!("dir to remove {} {}", dir_to_remove, dir_to_remove_size);
}
