// Day 2: Rock Paper Scissors (part 2)
//
// Find the total score playing rock paper scissors with
// (A, B, C) = rock, paper, scissors, (X, Y, Z) = you should lose, draw, win
use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

const ROCK: u32 = 1;
const PAPER: u32 = 2;
const SCISSORS: u32 = 3;

#[derive(Clone, Copy)]
enum Result {
    Lose,
    Draw,
    Win,
}

fn get_rps_score(opponent_play: u32, your_result: Result) -> u32 {
    match your_result {
        Result::Lose => return ((opponent_play + 1) % 3) + 1,
        Result::Draw => return opponent_play + 3,
        Result::Win => return (opponent_play % 3 + 1) + 6,
    }
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day2/input"
    } else {
        "assets/day2/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let lookup_opponent = HashMap::from([("A", ROCK), ("B", PAPER), ("C", SCISSORS)]);
    let lookup_you = HashMap::from([("X", Result::Lose), ("Y", Result::Draw), ("Z", Result::Win)]);

    let mut total_score = 0u32;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let line: Vec<&str> = line.split(' ').collect();

        total_score += get_rps_score(lookup_opponent[line[0]], lookup_you[line[1]]);
    }

    println!("total score is {}", total_score);
}
