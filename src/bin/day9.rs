// Day 9: Rope Bridge
//
// Find how many grid points the tail of a rope being dragged touches
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::str::FromStr;
use std::collections::HashSet;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day9/input"
    } else {
        "assets/day9/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut visited = HashSet::new();

    let mut rope_state = [0i32, 0i32];
    let mut tail_location = [0i32, 0i32];
    visited.insert(tail_location.clone());

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let line = line.split(" ").collect::<Vec::<&str>>();
        let m = match line[0] {
            "U" => [0, -1],
            "L" => [-1, 0],
            "D" => [0, 1],
            "R" => [1, 0],
            _ => panic!(),
        };
        let n = u32::from_str(line[1]).unwrap();

        for _i in 0..n {
            rope_state = [m[0] + rope_state[0], m[1] + rope_state[1]];

            if rope_state[0].abs() == 2 {
                tail_location = [tail_location[0] + rope_state[0]/2, tail_location[1] + rope_state[1]];
                visited.insert(tail_location.clone());
                rope_state = [rope_state[0]/2, rope_state[1]/2];
            } else if rope_state[1].abs() == 2 {
                tail_location = [tail_location[0] + rope_state[0], tail_location[1] + rope_state[1]/2];
                visited.insert(tail_location.clone());
                rope_state = [rope_state[0]/2, rope_state[1]/2];
            }
        }
    }

    println!("{}", visited.len());
}
