// Day 3: Rucksack Reorganization
//
// Search through string and find character that in second half of string that occurs in first half
// of string
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn get_priority(c: char) -> u32 {
    let c = c as u32;

    if c > 0x60 {
        return c - 0x60;
    }

    return (c - 0x40) + 26;
}

fn populate_occurance_table(table: &mut [bool; 52], contents: &str) {
    for c in contents.chars() {
        table[(get_priority(c) - 1) as usize] = true;
    }
}

fn get_repeated_item_priority(table: &[bool; 52], contents: &str) -> u32 {
    for c in contents.chars() {
        if table[(get_priority(c) - 1) as usize] {
            return get_priority(c);
        }
    }

    panic!("no repeated item found");
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day3/input"
    } else {
        "assets/day3/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut priority_total = 0u32;

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        let (compartment1, compartment2) = line.split_at(line.len() / 2);

        let mut occurance_table = [false; 52];

        populate_occurance_table(&mut occurance_table, compartment1);

        priority_total += get_repeated_item_priority(&occurance_table, compartment2);
    }

    println!("priority total is {}", priority_total);
}
