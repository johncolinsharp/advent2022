// Day 23: Unstable Diffusion (part 2)
// Calculate the diffusion of elves and see how many steps are necessary for diffusion to stop
use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug, Eq, Hash, PartialEq)]
enum Direction {
    N,
    S,
    E,
    W,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let f = if env::var("ADVENT_TEST").is_ok() {
        File::open("assets/day23/input_test")?
    } else {
        File::open("assets/day23/input")?
    };
    let f = BufReader::new(f);

    let mut check_list = Vec::<(i32, i32)>::new();
    let mut elf_map = HashMap::<(i32, i32), (bool, HashSet<Direction>)>::new();
    let mut directions = VecDeque::from([Direction::N, Direction::S, Direction::W, Direction::E]);

    for (y, l) in f.lines().map(|l| l.unwrap()).enumerate() {
        for (x, c) in l.chars().enumerate() {
            match c {
                '#' => {
                    check_list.push((x as i32, y as i32));
                    elf_map.insert(
                        (x as i32, y as i32),
                        (
                            false,
                            HashSet::from([Direction::N, Direction::S, Direction::W, Direction::E]),
                        ),
                    );
                }
                '.' => (),
                _ => panic!(),
            }
        }
    }

    let mut steps = 0;
    while !check_list.is_empty() {
        for square in &check_list {
            if !elf_map.contains_key(square) {
                continue;
            }

            let mut south_possible = true;
            let mut east_possible = true;
            let mut north_possible = true;
            let mut west_possible = true;
            let mut neighbour_seen = false;

            for a in -1..=1 {
                for b in -1..=1 {
                    if a != 0 || b != 0 {
                        if let Some(_neighbour) = elf_map.get_mut(&(square.0 + a, square.1 + b)) {
                            neighbour_seen = true;
                            if a == -1 {
                                west_possible = false;
                            } else if a == 1 {
                                east_possible = false;
                            }

                            if b == -1 {
                                north_possible = false;
                            } else if b == 1 {
                                south_possible = false;
                            }
                        }
                    }
                }
            }

            let e = elf_map.get_mut(&(square.0, square.1)).unwrap();
            *e = (
                false,
                HashSet::from([Direction::N, Direction::S, Direction::E, Direction::W]),
            );
            if neighbour_seen
                && (north_possible || south_possible || east_possible || west_possible)
            {
                e.0 = true;
                if !north_possible {
                    e.1.remove(&Direction::N);
                }
                if !south_possible {
                    e.1.remove(&Direction::S);
                }
                if !east_possible {
                    e.1.remove(&Direction::E);
                }
                if !west_possible {
                    e.1.remove(&Direction::W);
                }
            }
        }

        // go through map, make new map (target) -> (vec of sources)
        let mut proposed_locations = HashMap::<(i32, i32), Vec<(i32, i32)>>::new();

        for (elf, v) in &elf_map {
            if v.0 && !v.1.is_empty() {
                for d in &directions {
                    if v.1.contains(d) {
                        let new_location = match d {
                            Direction::N => (elf.0, elf.1 - 1),
                            Direction::E => (elf.0 + 1, elf.1),
                            Direction::S => (elf.0, elf.1 + 1),
                            Direction::W => (elf.0 - 1, elf.1),
                        };
                        proposed_locations
                            .entry(new_location)
                            .and_modify(|v| v.push(*elf))
                            .or_insert_with(|| vec![*elf]);
                        break;
                    }
                }
            } else {
                proposed_locations
                    .entry(*elf)
                    .and_modify(|v| v.push(*elf))
                    .or_insert_with(|| vec![*elf]);
            }
        }

        // go through new map, if vec sources len 1, then elf moves so update elf map
        check_list.clear();

        for (target, src) in proposed_locations {
            if src.len() == 1 && src[0] != target {
                for a in -1..=1 {
                    for b in -1..=1 {
                        if a != 0 || b != 0 {
                            check_list.push((target.0 + a, target.1 + b));
                            check_list.push((src[0].0 + a, src[0].1 + b));
                        }
                    }
                }

                elf_map.remove(&src[0]);
                elf_map.insert(
                    target,
                    (
                        false,
                        HashSet::from([Direction::N, Direction::S, Direction::E, Direction::W]),
                    ),
                );
            }
        }

        let t = directions.pop_front().unwrap();
        directions.push_back(t);
        steps += 1;
    }

    let (mut min_x, mut max_x, mut min_y, mut max_y) = (i32::MAX, i32::MIN, i32::MAX, i32::MIN);

    let num_elves = elf_map.len() as i32;
    for e in elf_map.keys() {
        if e.0 < min_x {
            min_x = e.0;
        }
        if e.1 < min_y {
            min_y = e.1;
        }
        if e.0 > max_x {
            max_x = e.0;
        }
        if e.1 > max_y {
            max_y = e.1;
        }
    }

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if elf_map.contains_key(&(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }

    println!(
        "num empty squares: {} {} {} {} {}",
        max_x,
        min_x,
        max_y,
        min_y,
        (max_x - min_x + 1) * (max_y - min_y + 1) - num_elves
    );
    println!("steps: {}", steps);

    Ok(())
}
