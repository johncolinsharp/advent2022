// Day 15: Beacon Exclusion Zone (part 2)
// For a given row of a map, find the number of positions where
// a beacon cannot be present
use lazy_static::lazy_static;
use regex::Regex;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::BTreeSet;
use std::str::FromStr;
use std::cmp::Ordering;
use std::cmp;

#[derive(PartialEq, Eq, Clone, Debug)]
struct LinePoint {
    p: i64,
    start: bool,
}

impl Ord for LinePoint {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.p == other.p {
            if self.start == other.start {
                return Ordering::Equal;
            }

            if self.start == false {
                return Ordering::Greater;
            }

            return Ordering::Less;
        }

        return self.p.cmp(&other.p);
    }
}

impl PartialOrd for LinePoint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct LineSet {
    line_points: BTreeSet<LinePoint>,
    len: u64,
}

impl LineSet {
    fn new() -> Self {
        LineSet { line_points: BTreeSet::new(), len: 0 }
    }

    fn subtract(&mut self, segment: [i64; 2]) {
        // don't bother subtracting 0 length segments
        if segment[0] == segment[1] {
            return;
        }

        let start_point = LinePoint{p: segment[0], start: false};
        let end_point = LinePoint{p: segment[1], start: true};

        let mut mid = self.line_points.split_off(&start_point);
        let mut end = mid.split_off(&end_point);

        let mut prev = &start_point;
        // loop through encompassed points, adding segments that
        // will be double subtracted
        for p in mid.iter() {
            if p.start {
                self.len += (p.p - prev.p) as u64;
            } else {
                prev = p;
            }
        }

        if let Some(l) = mid.last() {
            // if mid points end with a complete line, add
            // the double counted portion from the end of this line
            // to the end of the segment just subtracted
            if !l.start {
                self.len += (end_point.p - l.p) as u64;
            }
        } else if let Some(l) = self.line_points.last() {
            // if no midpoints, and the start section ends with
            // a line ending, then the line segment just subtracted
            // completely misses, add its entire length
            // since double counted
            if !l.start {
                self.len += (end_point.p - start_point.p) as u64;
            }
        } else {
            self.len += (end_point.p - start_point.p) as u64;
        }

        if self.len >= (end_point.p - start_point.p) as u64 {
            self.len -= (end_point.p - start_point.p) as u64;
        }

        if let Some(l) = self.line_points.last() {
            // if added segment added in vacant section, can add
            // segment's start point to start section
            if l.start {
                if l.p == start_point.p {
                    self.line_points.pop_last().unwrap();
                } else {
                    self.line_points.insert(start_point);
                }
                // println!("doing insert {:?}", self.line_points);
            }
        } else {
            // if added segment starts after all previous segments,
            // can just add start point on end
            // self.line_points.insert(start_point);
        }

        if let Some(f) = end.first() {
            // if end segment begins with a line starting, can
            // just add added segments end point to end section
            if !f.start {
                if f.p == end_point.p {
                    end.pop_first().unwrap();
                } else {
                    end.insert(end_point);
                }
            }
        }

        // append start and end, and so remove middle
        self.line_points.append(&mut end);
    }

    fn insert(&mut self, segment: [i64; 2]) {
        // don't bother adding 0 length segments
        if segment[0] == segment[1] {
            return;
        }

        let start_point = LinePoint{p: segment[0], start: true};
        let end_point = LinePoint{p: segment[1], start: false};

        // add line segment length to total length, will subtract double
        // counted sections in what follows
        self.len += (end_point.p - start_point.p) as u64;

        // println!("before {:?}", self.line_points);
        let mut mid = self.line_points.split_off(&start_point);
        let mut end = mid.split_off(&end_point);

        let mut prev = &start_point;
        // loop through encompassed points, subtracting segments that
        // have been double counted
        for p in mid.iter() {
            if p.start {
                prev = p;
            } else {
                self.len -= (p.p - prev.p) as u64;
            }
        }

        if let Some(l) = mid.last() {
            // if mid points end with an incomplete line, subtract
            // the double counted portion from the start of this line
            // to the end of the segment just added
            if l.start {
                self.len -= (end_point.p - l.p) as u64;
            }
        } else if let Some(l) = self.line_points.last() {
            // if no midpoints, and the start section ends with
            // a line starting, then the line segment just added is
            // completely encompassed, subtract its entire length
            // since double counted
            if l.start {
                self.len -= (end_point.p - start_point.p) as u64;
            }
        }

        if let Some(l) = self.line_points.last() {
            // if added segment added in vacant section, can add
            // segment's start point to start section
            if !l.start {
                if l.p == start_point.p {
                    self.line_points.pop_last().unwrap();
                } else {
                    self.line_points.insert(start_point);
                }
            }
        } else {
            // if added segment starts after all previous segments,
            // can just add start point on end
            self.line_points.insert(start_point);
        }

        if let Some(f) = end.first() {
            // if end segment begins with a line starting, can
            // just add added segments end point to end section
            if f.start {
                if f.p == end_point.p {
                    end.pop_first().unwrap();
                } else {
                    end.insert(end_point);
                }
            }
        } else {
            // if added segment ends after all previous segments,
            // can just add end point on end
            end.insert(end_point);
        }

        // append start and end, and so remove middle
        self.line_points.append(&mut end);
    }
}

struct SensorInfo {
    loc: [i64; 2],
    manhattan_d: u64,
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day15/input"
    } else {
        "assets/day15/input_test"
    };

    let search_limit = if env::var("ADVENT_TEST").is_err() {
        4000000
    } else {
        20
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);
    let mut sensor_infos = Vec::<SensorInfo>::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Sensor at x=(\-?\d+), y=(\-?\d+): closest beacon is at x=(\-?\d+), y=(\-?\d+)").unwrap();
        }
        let captures = RE.captures(&line).unwrap();

        let sensor_loc = [i64::from_str(&captures[1]).unwrap(),
            i64::from_str(&captures[2]).unwrap()];
        let beacon_loc = [i64::from_str(&captures[3]).unwrap(),
            i64::from_str(&captures[4]).unwrap()];

        let manhattan_d = sensor_loc[0].abs_diff(beacon_loc[0]) + sensor_loc[1].abs_diff(beacon_loc[1]);
        sensor_infos.push(SensorInfo{loc: sensor_loc, manhattan_d});
    }

    for y in 0..=search_limit {
        let mut poss_locations = LineSet::new();
        poss_locations.insert([0, search_limit]);
        for sensor_info in &sensor_infos {
            if sensor_info.loc[1].abs_diff(y) < sensor_info.manhattan_d {
                let x_lim = sensor_info.manhattan_d.abs_diff(sensor_info.loc[1].abs_diff(y)) as i64;
                poss_locations.subtract([cmp::max(0, sensor_info.loc[0]-x_lim), cmp::min(search_limit, sensor_info.loc[0]+x_lim+1)]);
                if poss_locations.len == 0 {
                    break;
                }
            }
        }

        if poss_locations.len > 0 {
            println!("tuning frequency {}", poss_locations.line_points.pop_first().unwrap().p * 4000000 + y);
            break;
        }
    }
}
