// Day 3: Rucksack Reorganization (part 2)
//
// Search in groups of three lines and find the 'priority' of the only shared 'item' between the
// three
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn get_priority(c: char) -> u32 {
    let c = c as u32;

    if c > 0x60 {
        return c - 0x60;
    }

    return (c - 0x40) + 26;
}

fn populate_occurance_table(table: &mut [u32; 52], contents: &str, flag: u32) {
    for c in contents.chars() {
        table[(get_priority(c) - 1) as usize] |= flag;
    }
}

fn get_badge_priority(table: &[u32; 52], contents: &str) -> u32 {
    for c in contents.chars() {
        if table[(get_priority(c) - 1) as usize] == 0b11 {
            return get_priority(c);
        }
    }

    panic!("no repeated item found");
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day3/input"
    } else {
        "assets/day3/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut priority_total = 0u32;

    let mut occurance_table = [0u32; 52];
    for (line, elf_no) in reader.lines().zip((1..=3).cycle()) {
        let line = line.expect("could not read line").to_string();

        match elf_no {
            1 => populate_occurance_table(&mut occurance_table, &line, 0b1),
            2 => populate_occurance_table(&mut occurance_table, &line, 0b10),
            3 => {
                priority_total += get_badge_priority(&occurance_table, &line);
                occurance_table = [0u32; 52];
            }
            _ => unreachable!(),
        }
    }

    println!("priority total is {}", priority_total);
}
