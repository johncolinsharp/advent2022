// Day 8: Treetop Tree House
//
// Find the number of 'visible' (taller than any tree left, right, up or down from it) trees in a
// grid
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day8/input"
    } else {
        "assets/day8/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut grid = Vec::<Vec<u8>>::new();

    for line in reader.lines() {
        let line = line.expect("could not read line").to_string();

        grid.push(
            line.chars().map(|c| c.to_digit(10).unwrap() as u8)
            .collect::<Vec<u8>>());
    }

    let mut visibilities = vec![vec![[true; 4]; grid[0].len()]; grid.len()];
    let mut max_seens = [0u8; 4];

    for y in 0..grid.len() {
        for x in 0..grid[0].len() {

            // set whether tree visible from left
            if x == 0 {
                max_seens[0] = grid[y][x];
            } else {
                if grid[y][x] > max_seens[0] {
                    max_seens[0] = grid[y][x];
                } else {
                    visibilities[y][x][0] = false;
                }
            }

            // set whether tree visible from right
            let l = grid[0].len()-1;
            let x_p = l - x;
            if x_p == l {
                max_seens[1] = grid[y][x_p];
            } else {
                if grid[y][x_p] > max_seens[1] {
                    max_seens[1] = grid[y][x_p];
                } else {
                    visibilities[y][x_p][1] = false;
                }
            }

        }
    }

    for x in 0..grid[0].len() {
        for y in 0..grid.len() {
            // set whether tree visible from top
            if y == 0 {
                max_seens[2] = grid[y][x];
            } else {
                if grid[y][x] > max_seens[2] {
                    max_seens[2] = grid[y][x];
                } else {
                    visibilities[y][x][2] = false;
                }
            }

            // set whether tree visible from bottom
            let l = grid.len()-1;
            let y_p = l - y;
            if y_p == l {
                max_seens[3] = grid[y_p][x];
            } else {
                if grid[y_p][x] > max_seens[3] {
                    max_seens[3] = grid[y_p][x];
                } else {
                    visibilities[y_p][x][3] = false;
                }
            }

        }
    }

    let mut total_trees_visible = 0;
    for x in 0..grid[0].len() {
        for y in 0..grid.len() {
            let v = &visibilities[y][x];
            if v[0] == true || v[1] == true || v[2] == true || v[3] == true {
                total_trees_visible += 1;
            }
        }
    }

    println!("total trees visible: {}", total_trees_visible);
}
