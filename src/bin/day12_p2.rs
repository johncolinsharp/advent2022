// Day 12: Hill Climbing Algorithm (part 2)
// Calculate shortest distance from any point elevation 'a' to E
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
struct Node {
    h: u8,
    shortest_dist: u32,
    is_end: bool,
}

fn main() {
    let file_path = if env::var("ADVENT_TEST").is_err() {
        "assets/day12/input"
    } else {
        "assets/day12/input_test"
    };

    let fp = File::open(file_path).expect("Could not open file");
    let reader = BufReader::new(fp);

    let mut nodes = Vec::<Vec<Node>>::new();
    let mut to_visit = HashSet::<[usize; 2]>::new();
    let mut end_node: Option<[usize; 2]> = None;

    for (i, line) in reader.lines().enumerate() {
        let line = line.expect("could not read line").to_string();

        nodes.push(
            line.chars()
                .enumerate()
                .map(|(j, c)| {
                    to_visit.insert([j, i]);
                    if c == 'S' {
                        Node {
                            h: 0u8,
                            shortest_dist: u32::MAX,
                            is_end: true,
                        }
                    } else if c == 'E' {
                        end_node = Some([j, i]);
                        Node {
                            h: 25u8,
                            shortest_dist: 0,
                            is_end: false,
                        }
                    } else {
                        let h = (c as u8) - 'a' as u8;
                        Node {
                            h: h,
                            shortest_dist: u32::MAX,
                            is_end: if h == 0 { true } else { false },
                        }
                    }
                })
                .collect::<Vec<Node>>(),
        );
    }

    loop {
        let mut min = u32::MAX;
        let mut x = 0;
        let mut y = 0;
        for [x_t, y_t] in to_visit.iter() {
            if nodes[*y_t][*x_t].shortest_dist < min {
                x = *x_t;
                y = *y_t;
                min = nodes[*y_t][*x_t].shortest_dist;
            }
        }
        to_visit.remove(&[x, y]);

        if nodes[y][x].is_end == true {
            println!("shortest distance is {:?}", nodes[y][x].shortest_dist);
            break;
        }

        if y + 1 < nodes.len() {
            if nodes[y][x].h <= nodes[y + 1][x].h + 1 {
                if nodes[y + 1][x].shortest_dist > nodes[y][x].shortest_dist + 1 {
                    nodes[y + 1][x].shortest_dist = nodes[y][x].shortest_dist + 1;
                }
            }
        }
        if y > 0 {
            if nodes[y][x].h <= nodes[y - 1][x].h + 1 {
                if nodes[y - 1][x].shortest_dist > nodes[y][x].shortest_dist + 1 {
                    nodes[y - 1][x].shortest_dist = nodes[y][x].shortest_dist + 1;
                }
            }
        }
        if x + 1 < nodes[0].len() {
            if nodes[y][x].h <= nodes[y][x + 1].h + 1 {
                if nodes[y][x + 1].shortest_dist > nodes[y][x].shortest_dist + 1 {
                    nodes[y][x + 1].shortest_dist = nodes[y][x].shortest_dist + 1;
                }
            }
        }
        if x > 0 {
            if nodes[y][x].h <= nodes[y][x - 1].h + 1 {
                if nodes[y][x - 1].shortest_dist > nodes[y][x].shortest_dist + 1 {
                    nodes[y][x - 1].shortest_dist = nodes[y][x].shortest_dist + 1;
                }
            }
        }
    }
}
